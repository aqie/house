package com.aqie.house.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@EnableDiscoveryClient
@EnableCircuitBreaker // 启用断路器
@SpringBootApplication
@Controller
//@RibbonClient(name="user",configuration=NewRuleConfig.class)
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    // 获取用户注册实例
    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping("userInstance")
    @ResponseBody
    public List<ServiceInstance> getUserRegister(){
        return discoveryClient.getInstances("user");
    }

    @RequestMapping("gatewayInstance")
    @ResponseBody
    public List<ServiceInstance> getRegister(){
        return discoveryClient.getInstances("gateway");
    }
}
