package com.aqie.house.gateway;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/23 14:22
 */
public class Test {
    public static void main(String[] args) {
        List<String> paths = Lists.newArrayList();
        String filePath = "D:\\gitee2\\java\\springcloud\\microHouse\\gateway\\uploads";
        File localFile = new File("D:/gitee2/java/springcloud/microHouse/gateway/uploads/1553324353/b4.jpg");
        String absolutePath = localFile.getAbsolutePath();
        String path = StringUtils.substringAfterLast(absolutePath, filePath);
        paths.add(path);
        String avatar = paths.get(0);
        System.out.println(avatar);
    }
}
