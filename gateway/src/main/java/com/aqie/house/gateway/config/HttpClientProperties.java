package com.aqie.house.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/21 19:02
 */
@ConfigurationProperties(prefix="spring.httpclient")

public class HttpClientProperties {
    private Integer connectTimeOut = 1000;  // 1s
    private Integer socketTimeOut = 10000;

    private String agent = "agent";
    private Integer maxConnPerRoute = 10;   // 最大连接数
    private Integer maxConnTotal = 50;     // 总连接数

    public Integer getConnectTimeOut() {
        return connectTimeOut;
    }
    public void setConnectTimeOut(Integer connectTimeOut) {
        this.connectTimeOut = connectTimeOut;
    }
    public Integer getSocketTimeOut() {
        return socketTimeOut;
    }
    public void setSocketTimeOut(Integer socketTimeOut) {
        this.socketTimeOut = socketTimeOut;
    }
    public String getAgent() {
        return agent;
    }
    public void setAgent(String agent) {
        this.agent = agent;
    }
    public Integer getMaxConnPerRoute() {
        return maxConnPerRoute;
    }
    public void setMaxConnPerRoute(Integer maxConnPerRoute) {
        this.maxConnPerRoute = maxConnPerRoute;
    }
    public Integer getMaxConnTotal() {
        return maxConnTotal;
    }
    public void setMaxConnTotal(Integer maxConnTotal) {
        this.maxConnTotal = maxConnTotal;
    }
}
