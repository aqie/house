package com.aqie.house.gateway.controller;

import com.aqie.house.gateway.common.CommonConstants;
import com.aqie.house.gateway.common.PageData;
import com.aqie.house.gateway.common.PageParams;
import com.aqie.house.gateway.model.Blog;
import com.aqie.house.gateway.model.Comment;
import com.aqie.house.gateway.model.House;
import com.aqie.house.gateway.service.CommentService;
import com.aqie.house.gateway.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/24 15:52
 */
@Controller
public class BlogController {


    @Autowired
    private CommentService commentService;

    @Autowired
    private HouseService houseService;


    @RequestMapping(value="blog/list",method={RequestMethod.POST,RequestMethod.GET})
    public String list(Integer pageSize, Integer pageNum, Blog query, ModelMap modelMap){
        PageData<Blog> ps = commentService.queryBlogs(query, PageParams.build(pageSize, pageNum));
        List<House> houses =  houseService.getHotHouse(CommonConstants.RECOM_SIZE);
        modelMap.put("recomHouses", houses);
        modelMap.put("ps", ps);
        return "/blog/listing";
    }

    @RequestMapping(value="blog/detail",method={RequestMethod.POST,RequestMethod.GET})
    public String blogDetail(int id,ModelMap modelMap){
        Blog blog = commentService.queryOneBlog(id);
        List<Comment> comments = commentService.getBlogComments(id);
        List<House> houses =  houseService.getHotHouse(CommonConstants.RECOM_SIZE);
        modelMap.put("recomHouses", houses);
        modelMap.put("blog", blog);
        modelMap.put("commentList", comments);
        return "/blog/detail";
    }
}
