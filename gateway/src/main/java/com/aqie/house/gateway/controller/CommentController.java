package com.aqie.house.gateway.controller;

import com.aqie.house.gateway.common.UserContext;
import com.aqie.house.gateway.model.User;
import com.aqie.house.gateway.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/24 15:58
 */
@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;


    @RequestMapping(value="comment/leaveComment")
    public String leaveComment(String content, Long houseId, ModelMap modelMap){
        User user = UserContext.getUser();
        Long userId =  user.getId();
        commentService.addHouseComment(houseId,content,userId);
        return "redirect:/house/detail?id=" + houseId;
    }

    @RequestMapping(value="comment/leaveBlogComment")
    public String leaveBlogComment(String content,Integer blogId,ModelMap modelMap){
        User user = UserContext.getUser();
        Long userId =  user.getId();
        commentService.addBlogComment(blogId,content,userId);
        return "redirect:/blog/detail?id=" + blogId;
    }

}
