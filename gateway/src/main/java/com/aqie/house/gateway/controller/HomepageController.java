package com.aqie.house.gateway.controller;

import com.aqie.house.gateway.model.House;
import com.aqie.house.gateway.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/23 17:03
 */

@Controller
public class HomepageController {

    @Autowired
    private HouseService houseService;

    @RequestMapping("index")
    public String accountsRegister(ModelMap modelMap){
        List<House> houses =  houseService.getLastest();
        modelMap.put("recomHouses", houses);
        return "/homepage/index";
    }

    @RequestMapping("")
    public String index(ModelMap modelMap){
        return "redirect:/index";
    }
}
