package com.aqie.house.gateway.helper;

import com.aqie.house.gateway.common.ResultMsg;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/24 15:57
 */
public class ControllerHelper {

    public static void addFlashAttributes(RedirectAttributes redirectAttributes, Map<String, String> map){
        map.forEach((key,value)-> redirectAttributes.addFlashAttribute(key, value));
    }

    public static void addFlashAttributes(RedirectAttributes redirectAttributes, ResultMsg resultMsg){
        addFlashAttributes(redirectAttributes, resultMsg.asMap());
    }

}
