package com.aqie.house.gateway.inteceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/23 16:55
 */
@Configuration
public class WebMvcConf extends WebMvcConfigurerAdapter {

    @Autowired
    private AuthInterceptor authInterceptor;

    @Autowired
    private AuthActionInterceptor authActionInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor).excludePathPatterns("/static").addPathPatterns("/**");
        registry
                .addInterceptor(authActionInterceptor)
                .addPathPatterns("/house/toAdd")
                .addPathPatterns("/accounts/profile").addPathPatterns("/accounts/profileSubmit")
                .addPathPatterns("/house/bookmarked").addPathPatterns("/house/del")
                .addPathPatterns("/house/ownlist").addPathPatterns("/house/add")
                .addPathPatterns("/house/toAdd").addPathPatterns("/agency/agentMsg")
                .addPathPatterns("/comment/leaveComment").addPathPatterns("/comment/leaveBlogComment");

        super.addInterceptors(registry);
    }

    // 全局支持cors
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        super.addCorsMappings(registry);
        // 校验哪些域名
        registry.addMapping("/**")
                // 放行哪些域名
                .allowedOrigins("*")
                // 是否允许发送cookie
                .allowCredentials(true)
                // 放行哪些http 方法
                .allowedMethods("GET","POST","PUT","DELETE")
                // 预检请求， 放行哪些header
                .allowedHeaders("*")
                // 暴露那些头部信息
                .exposedHeaders("Header1","Header2");
    }
}

