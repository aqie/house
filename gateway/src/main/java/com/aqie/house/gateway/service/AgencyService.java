package com.aqie.house.gateway.service;

import com.aqie.house.gateway.common.PageData;
import com.aqie.house.gateway.common.PageParams;
import com.aqie.house.gateway.dao.UserDao;
import com.aqie.house.gateway.model.Agency;
import com.aqie.house.gateway.model.ListResponse;
import com.aqie.house.gateway.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/23 10:39
 */
@Service
public class AgencyService {

    @Autowired
    private UserDao userDao;


    public List<Agency> getAllAgency(){
        return userDao.getAllAgency();
    }

    public Agency getAgency(Integer id){
        return userDao.getAgencyById(id);
    }

    public void add(Agency agency) {
        userDao.addAgency(agency);
    }

    public PageData<User> getAllAgent(PageParams pageParams) {
        ListResponse<User> result =  userDao.getAgentList(pageParams.getLimit(),pageParams.getOffset());
        Long  count  =  result.getCount();
        return PageData.<User>buildPage(result.getList(), count, pageParams.getPageSize(), pageParams.getPageNum());
    }



    public User getAgentDetail(Long id) {
        return userDao.getAgentById(id);
    }

}
