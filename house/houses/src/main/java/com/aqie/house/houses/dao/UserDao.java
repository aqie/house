package com.aqie.house.houses.dao;

import com.aqie.house.houses.common.RestResponse;
import com.aqie.house.houses.model.User;
import com.aqie.house.houses.service.GenericRest;
import com.aqie.house.houses.utils.Rests;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/23 19:33
 */
@Repository
public class UserDao {

    @Autowired
    private GenericRest rest;

    @Value("${user.service.name}")
    private String userServiceName;

    public User getAgentDetail(Long agentId) {
        RestResponse<User> response = Rests.exc(() -> {
            String url = Rests.toUrl(userServiceName, "/agency/agentDetail" + "?id=" + agentId);
            ResponseEntity<RestResponse<User>> responseEntity = rest.get(url,
                    new ParameterizedTypeReference<RestResponse<User>>() {});
            return responseEntity.getBody();
        });
        return response.getResult();
    }




}
