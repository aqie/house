package com.aqie.house.houses.mapper;

import com.aqie.house.houses.model.City;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface CityMapper {
  
  public List<City> selectCitys(City city);

}
