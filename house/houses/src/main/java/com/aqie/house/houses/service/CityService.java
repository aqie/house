package com.aqie.house.houses.service;

import com.aqie.house.houses.mapper.CityMapper;
import com.aqie.house.houses.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/23 19:34
 */
@Service
public class CityService {

    @Autowired
    private CityMapper cityMapper;

    public List<City> getAllCitys(){
        City query = new City();
        return cityMapper.selectCitys(query);
    }

}
