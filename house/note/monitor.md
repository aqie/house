#### port
- gateway       : 80
- gateway_actuator : 7777
- fileServer    : 9999
- eureka        : 8000
- admin         : 9000

- user          : 81,82
- user_actuator : 8888
- houses        ：83
- house_actuator: 8889
- comment       ：84
- comment_actuator:8890

- hystrix-dashboard : 85
#### Druid
- [druid](http://localhost:81/druid/index.html)
-
#### Actuator
+ user
    - [health](http://localhost:8888/actuator/health)
    - management.server.port
+ gateway
    - [health](http://localhost:7777/actuator/health)
    -    

#### Admin
-
-

#### Eureka
- [eureka](http://localhost:8000/)

#### fileServer
- 使用 nginx 代理