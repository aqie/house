### maven
- mvn spring-boot:run -Dspring.profiles.active=dev

#### 微服务脚手架
- 服务注册发现组件
- 负载均衡组件
- 服务通信组件
- 数据访问层
- 异常统一处理
    + GlobalExceptionHandler
    + Exception2CodeRepo 
    + IllegalParamsException
    + WithTypeException 
- http 日志组件, json解析器

#### 跨域
- CORS(cross-origin resources sharing
    + 通过添加Response Header 实现
    + Jsonp 只支持get, CORS 不支持老浏览器
- CORS 头部定义
    + Access-Control-Allow-Origin   : 检测域名存在则返回该头
    + Access-Control-Allow-Origin   ：是否允许发送(目标域名下))cookie，默认false
    + Access-Control-Expose-Headers : 列举浏览器允许的其他header
- springboot 支持跨域
    + 法1：覆盖WebMvcConfigureAdapter 中 addCorsMappings
        * com.aqie.house.gateway.inteceptor.WebMvcConf
    + 法2：Controller 添加 @CrossOrigin
- com.aqie.house.gateway.inteceptor.AuthInterceptor : 支持cookie
    + 
- CORS 两种请求