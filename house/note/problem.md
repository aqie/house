##### 数据库
- Failed to configure a DataSource: 'url' attribute is not specified and no embedded datasource could be configured.
    + 引入mybatis 依赖 没有添加其对应配置
    + java 文件下配置 mapper接口，resources下配置xml映射文件
    + DruidConfig : 缺少配置文件
-   
#### actuator
- 无法访问 ： 缺少配置  
    + 访问路径：http://localhost:8888/actuator/health
        * 添加配置查看所有
    + user application.yml 配置
- healthcheck 
    +  健康上报 如果mysql停掉, user服务也会下线

#### admin
- java.lang.IllegalStateException: Calling [asyncError()] is not valid for a request with Async state [MUST_DISPATCH]
    +     

#### lombok
- Class path contains multiple SLF4J bindings.
    + 删除指定目录
- Failed to load class "org.slf4j.impl.StaticLoggerBinder".    
    + 添加依赖，然后就会报上面的错
- Logging system failed to initialize using configuration from 'classpath:log4j2.xml'
    + 
- Failed to load class "org.slf4j.impl.StaticLoggerBinder"
    + 缺少 log4j2 配置        
    
#### 编译启动
- Command line is too long.
    +     

#### Mybatis
- org.apache.ibatis.binding.BindingException: Invalid bound statement (not found)
    + mapper.house.xml 命名空间错误    

#### Request
- org.springframework.http.converter.HttpMessageNotReadableException: JSON parse error: Invalid UTF-8 start byte 0x88; nested exception is com.fasterxml.jackson.core.JsonParseException: Invalid UTF-8 start byte 0x88 
- com.fasterxml.jackson.core.JsonParseException: Invalid UTF-8 start byte 0x88
    + json 字符串格式问题 
- com.aqie.house.houses.utils.RestException: sendReq error
    + 没有启动user 服务
    