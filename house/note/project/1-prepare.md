### 项目初始化
- Slf4j 日志框架
- 过滤器配置
- 内嵌servlet容器jetty 替换tomcat
- 整合mybatis
    + xml
        * datasource 配置
        * 创建SqlSessionFactoryBean,MapperScannerConfigurer bean
        * 创建mybatis配置文件 mapper接口类 及SQL映射文件
    + starter
        * mybatis starter
        * application 配置
        * mybatis 配置文件 mapper接口类 + sql映射文件
- 集成Druid连接池
    + com.aqie.house.config.DruidConfig
- 整合freemarker
    + xml
        * pom 引入freemarker库
        * freemarkerConfig viwResolver bean
        * 编写模板引擎文件
    + starter
        * freemarker starter
        * application 配置
        * 编写模板引擎文件
    + freemarker 结构化布局
### 引入内嵌容器
- 引入tomcat : 直接引入spring-boot-starter-web,默认 tomcat
- 引入jetty : 添加 spring-boot-starter-jetty 并移除starter
### 整合mybatis
- 添加maven依赖 + application 配置
- 配置文件 ： mybatis/mybatis-config.xml
- SQL文件 ： mapper/user.xml
- 接口类 ： com.aqie.house.mapper.UserMapper
- service : 注入 UserMapper
- controller ： 
### springStarter
- springBoot
    * spring-core
    * spring-context
- spring-boot-autoconfigure
- spring-boot-starter-logging
### mybatis-spring-boot-starter
- mybatis
- mybatis-spring
- spring-boot-starter-jdbc
- mybatis-spring-boot-autoconfigure
### 自定义SpringBoot 起步依赖 (httpClient 自动配置)
- 方法一 ：META-INF/spring.factories 修改包名 通过这个配置注入
- 方法二 ：coma.aqie.house.autoconfig.EnableHttpClient 并在启动类添加注解
