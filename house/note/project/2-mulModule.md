### maven 多Module 拆分
- house-biz : 数据访问层及业务实现
    + config
    + mapper
    + service
    + resources/mapper
    + resources/mybatis
    + 依赖
        * mysql
        * druid
        * mail
        * mybatis
        * house-common
- house-common : 定义数据模型及公共代码
    + model
    + utils
    + 依赖
        * lombok
        * guava
        * httpClient
        * commons-lang3
- house-web : 负责启动类,controller 模板引擎
    + controller
    + filter
    + interceptor
    + autoconfig
    + resources/static
    + resources/templates
    + resources/logback-spring.xml
    + resources/META-INF
    + 依赖    
        * lombok
        * freemarker
        * house-biz
### 修改