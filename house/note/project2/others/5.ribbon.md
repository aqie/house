#### Ribbon
- 客户端负载均衡器
- 集成在eureka依赖中
- lbRestTemplate bean 添加 loadBalance

#### problem
- 如何配置不同的负载均衡策略
- 脱离eureka 配置ribbon
- 动态删除下线的服务列表