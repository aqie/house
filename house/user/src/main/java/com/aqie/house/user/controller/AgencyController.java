package com.aqie.house.user.controller;

import com.aqie.house.user.common.PageParams;
import com.aqie.house.user.common.RestResponse;
import com.aqie.house.user.model.Agency;
import com.aqie.house.user.model.ListResponse;
import com.aqie.house.user.model.User;
import com.aqie.house.user.service.AgencyService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/21 19:59
 */
@RestController
@RequestMapping("agency")
public class AgencyController {

    @Autowired
    private AgencyService agencyService;

    @PostMapping("add")
    public RestResponse<Object> addAgency(@RequestBody Agency agency) {
        agencyService.add(agency);
        return RestResponse.success();
    }

    @GetMapping("list")
    public RestResponse<List<Agency>> agencyList() {
        List<Agency> agencies = agencyService.getAllAgency();
        return RestResponse.success(agencies);
    }


    @GetMapping("agentList")
    public RestResponse<ListResponse<User>> agentList(Integer limit, Integer offset) {
        PageParams pageParams = new PageParams();
        pageParams.setLimit(limit);
        pageParams.setOffset(offset);
        Pair<List<User>, Long> pair = agencyService.getAllAgent(pageParams);
        ListResponse<User> response = ListResponse.build(pair.getKey(), pair.getValue());
        return RestResponse.success(response);
    }

    @GetMapping("agentDetail")
    public RestResponse<User> agentDetail(Long id) {
        User user = agencyService.getAgentDetail(id);
        return RestResponse.success(user);
    }

    @GetMapping("agencyDetail")
    public RestResponse<Agency> agencyDetail(Integer id) {
        Agency agency = agencyService.getAgency(id);
        return RestResponse.success(agency);
    }

}
