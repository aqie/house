package com.aqie.house.user.controller;

import com.aqie.house.user.common.RestResponse;
import com.aqie.house.user.model.User;
import com.aqie.house.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/21 19:59
 */
@RestController
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private UserService userService;

    @Value("${server.port}")
    private Integer port;

    @GetMapping("test")
    public RestResponse<String> test(){
        redisTemplate.opsForValue().set("name", "aqie");
        return RestResponse.success("test " + port);
    }


    //-------------------查询---------------------

    @GetMapping("getById")
    public RestResponse<User> getUserById(Long id){
        User user = userService.getUserById(id);
        return RestResponse.success(user);
    }

    @PostMapping("getList")
    public RestResponse<List<User>> getUserList(@RequestBody User user){
        List<User> users = userService.getUserByQuery(user);
        return RestResponse.success(users);
    }


    //----------------------注册----------------------------------
    @PostMapping("add")
    public RestResponse<User> add(@RequestBody User user){
        userService.addAccount(user,user.getEnableUrl());
        return RestResponse.success();
    }

    /**
     * 主要激活key的验证
     */
    @GetMapping("enable")
    public RestResponse<Object> enable(String key){
        userService.enable(key);
        return RestResponse.success();
    }

    //------------------------登录/鉴权--------------------------

    @PostMapping("auth")
    public RestResponse<User> auth(@RequestBody User user){
        User finalUser = userService.auth(user.getEmail(),user.getPasswd());
        return RestResponse.success(finalUser);
    }


    @GetMapping("get")
    public RestResponse<User> getUser(String token){
        User finalUser = userService.getLoginedUserByToken(token);
        return RestResponse.success(finalUser);
    }

    @GetMapping("logout")
    public RestResponse<Object> logout(String token){
        userService.invalidate(token);
        return RestResponse.success();
    }

    @PostMapping("update")
    public RestResponse<User> update(@RequestBody User user){
        User updateUser = userService.updateUser(user);
        return RestResponse.success(updateUser);
    }

    @PostMapping("reset")
    public RestResponse<User> reset(String key ,String password){
        User updateUser = userService.reset(key,password);
        return RestResponse.success(updateUser);
    }

    @GetMapping("getKeyEmail")
    public RestResponse<String> getKeyEmail(String key){
        String  email = userService.getResetKeyEmail(key);
        return RestResponse.success(email);
    }

    @GetMapping("resetNotify")
    public RestResponse<User> resetNotify(String email,String url){
        userService.resetNotify(email,url);
        return RestResponse.success();
    }
}
