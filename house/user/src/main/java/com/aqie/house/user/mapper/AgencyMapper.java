package com.aqie.house.user.mapper;

import com.aqie.house.user.common.PageParams;
import com.aqie.house.user.model.Agency;
import com.aqie.house.user.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/21 17:20
 */
@Mapper
public interface AgencyMapper {

    List<Agency> select(Agency agency);

    int insert(Agency agency);

    List<User>	selectAgent(@Param("user") User user, @Param("pageParams") PageParams pageParams);

    Long selectAgentCount(@Param("user")User user);

}
