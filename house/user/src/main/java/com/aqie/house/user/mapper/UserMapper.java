package com.aqie.house.user.mapper;

import com.aqie.house.user.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/21 17:20
 */
@Mapper // 转换成bean
public interface UserMapper {

    public List<User> selectUsers();

    public List<User> selectUsersByQuery(User user);

    User selectById(Long id);

    List<User> select(User user);

    int update(User user);

    int insert(User account);

    int delete(String email);

    User selectByEmail(String email);
}

